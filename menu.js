function dropdown(id) {
    let e = document.getElementsByClassName("submenu");
    let g = document.getElementsByClassName("menu-arrow");
    let z = document.getElementById(id);
    for (let i = 0; i < e.length; i++) {
        if (e[i].id === id) {
            if (z.style.display === "block") {
                z.style.display = "none";
                document.getElementById("i" + id).style.transform = "rotate(-46deg)";
            } else {
                z.style.display = "block";
                document.getElementById("i" + id).style.transform = "rotate(45deg)";
            }
        } else {
            e[i].style.display = "none";
            g[i].style.transform = "rotate(-45deg)";
        }
    }
}